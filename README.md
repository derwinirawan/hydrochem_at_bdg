# Hydrochemistry of groundwater in Bandung 

A project to reproduce multivariate analysis by [Taufik et.al (2018)]( https://www.sciencedirect.com/science/article/abs/pii/S0043135418308467) using R, and also an attempt to remix the dataset with the previous data to gain more knowledge of Bandung's hydrogeological system.

![illustration](https://upload.wikimedia.org/wikipedia/commons/1/1b/Bivariate_example.png)

(Borrowed from [Wikipedia](https://en.wikipedia.org/wiki/Multivariate_kernel_density_estimation))